execute_process(COMMAND ${TEST_PROG} ${TEST_ARG1} ${TEST_ARG2}
                RESULT_VARIABLE HAD_ERROR)
if (HAD_ERROR)
    message(FATAL_ERROR "Test failed")
endif()

execute_process(COMMAND ${TEST_CMP} -dmf
    test_7.nc output_7.nc
    RESULT_VARIABLE DIFFERENT)
if (DIFFERENT)
    message(FATAL_ERROR "Test failed - files differ")
endif()
