#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>

#define _USE_MATH_DEFINES
#include <cmath>

#if defined(_OPENMP)
    #include <omp.h>
#endif //_OPENMP

#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/progress.hpp>

#include <netcdf>

#include "version.hpp"

using namespace std;
using namespace boost::program_options;
using namespace boost::iostreams;
using namespace netCDF;
using namespace netCDF::exceptions;

#define UNREFERENCED_PARAMETER(P) (P)

int main(int argc, char* argv[])
{
    using boost::lexical_cast;
    using boost::bad_lexical_cast;

    string output_file, to_ignore;
    vector<string> input_fields, input_indices;
    string config_file_path = "spins.conf";
#if defined(_OPENMP)
    unsigned num_threads;
#endif  //_OPENMP
    size_t Nx, Ny, Nz;
    double Lx, Ly, Lz;
    double min_x, min_y, min_z;
    double plot_interval;
    string type_x, type_y, type_z;
    bool mapped_grid;

    // disabling the synchronization before any other I/O operation
    ios_base::sync_with_stdio(false);

    options_description visible(
        "usage: spins2netcdf [ options ]\n"
        "                    [-o|--output-file] OUTPUT \n\n"
        "Allowed options");
    visible.add_options()
    ("help,h", "display this help and exit")
    ("version,v", "output version information and exit")
    ("input-indices,i", value< vector<string> >(&input_indices), "input indices  (repeatable e.g. -i0 -i5 -i42)")
    ("input-fields,f", value< vector<string> >(&input_fields), "input fields  (repeatable e.g. -fu -fw -frho)")
    ("use-mmap-file,m", "use memory mapped file")
    ("keep-orient,k", "keep axes orientation")
#if defined(_OPENMP)
    ("num-threads,n", value<unsigned>(&num_threads)->default_value(omp_get_num_procs()), "number of threads to use for changing axes orientation")
#endif  //_OPENMP
    ("silent,s", "switch to silent mode")
    ("keep-temporary-files,t", "keep temporary files for debugging (i.e. *.tmp)");

    options_description hidden("Hidden options");
    hidden.add_options()
    ("output-file,o", value<string>(&output_file)->default_value("output"), "output file")
    ("Nx", value<size_t>(&Nx), "x size")
    ("Ny", value<size_t>(&Ny), "y size")
    ("Nz", value<size_t>(&Nz), "z size")
    ("Lx", value<double>(&Lx)->default_value(1), "length of x dimension")
    ("Ly", value<double>(&Ly)->default_value(1), "length of y dimension")
    ("Lz", value<double>(&Lz)->default_value(1), "length of z dimension")
    ("min_x", value<double>(&min_x)->default_value(0), "minimum x value")
    ("min_y", value<double>(&min_y)->default_value(0), "minimum y value")
    ("min_z", value<double>(&min_z)->default_value(0), "minimum z value")
    ("type_x", value<string>(&type_x), "x grid type")
    ("type_y", value<string>(&type_y), "y grid type")
    ("type_z", value<string>(&type_z), "z grid type")
    ("plot_interval", value<double>(&plot_interval), "time interval between successive outputs")
    ("mapped_grid", value<bool>(&mapped_grid)->default_value(false), "is grid mapped?");

    positional_options_description p;
    p.add("output-file", 1);

    options_description all("Allowed options");
    all.add(visible).add(hidden);

    variables_map vm;

    try
    {
        store(command_line_parser(argc, argv).options(all).positional(p).run(), vm);
        ifstream config_file(config_file_path.c_str());
        store(parse_config_file(config_file, all, true), vm);

        if (vm.count("version"))
        {
                cout << "spins2netcdf version " 
                     << SPINS_TOOLS_VERSION_MAJOR << '.'
                     << SPINS_TOOLS_VERSION_MINOR << '.'
                     << SPINS_TOOLS_VERSION_PATCH << '.'
                     << SPINS_TOOLS_VERSION_TWEAK << endl;
                return 0;
        }

        if (vm.count("help"))
        {
            cout << visible << endl;
            return 0;
        }

        // having notify after -v and -h options...
        notify(vm);

        // actual code starts here...
        if (!boost::filesystem::exists(config_file_path))
        {
            cerr << "Error: no spins.conf file found!"
                 << endl;
            return 1;
        }

        if (!vm.count("silent"))
            cout << "Grid size: "
                 << Nx << " x "
                 << Ny << " x "
                 << Nz << endl;

#if defined(_OPENMP)
        if (!vm.count("keep-orient")
        && !vm.count("use-mmap-file"))
        {
            omp_set_num_threads(num_threads);
            if (!vm.count("silent"))
                cout << "Using "
                     << omp_get_max_threads()
                     << " processor(s)"
                     << endl;
        }
#endif  //_OPENMP

        // creating files map
        map<string, vector<string> > filenames_map;
        boost::filesystem::directory_iterator end_itr;
        for (boost::filesystem::directory_iterator itr(".")
        ;   itr != end_itr
        ;   ++itr)
        {
            if (!boost::filesystem::is_regular_file(itr->status()))
                continue;
            string file_extension = itr->path().extension().string();
            file_extension.erase(0, 1);
            if (isdigit(file_extension[0]))
            {
                //cout << itr->path().filename().string() << endl;
                map<string, vector<string> >::iterator it = filenames_map.find(file_extension);
                if (it != filenames_map.end())
                    it->second.push_back(itr->path().stem().string());
                else
                {
                    vector<string> file_names;
                    file_names.push_back(itr->path().stem().string());
                    filenames_map.insert(pair<string, vector<string> >(file_extension, file_names));
                }
            }
        }

        if (!vm.count("silent"))
            cout << "Number of files to write: "
                 << (vm.count("input-indices")
                 ?  input_indices.size()
                 :  filenames_map.size())
                 << endl;

        for (map<string, vector<string> >::iterator itr = filenames_map.begin()
        ;   itr != filenames_map.end()
        ;   ++itr)
        {
            // skiping input indices that are not requested
            if (vm.count("input-indices")
            &&  find(input_indices.begin()
            ,   input_indices.end()
            ,   itr->first)
            ==  input_indices.end())
                continue;

            // Create the file
            string ofile_name = output_file + '_' + itr->first + ".nc";
            if (!vm.count("silent"))
                cout << "Writing "
                     << ofile_name
                     << endl;
            NcFile output(ofile_name, NcFile::replace);
            ostringstream desc;
            desc << "SPINS Output " << itr->first;
            output.putAtt("description", desc.str());
            if (mapped_grid)
                output.putAtt("grid_type", "curvilinear");
            else
                output.putAtt("grid_type", "rectilinear");


            // Define the dimensions
            // NetCDF will hand back an ncDim object for each
            NcDim x_dim, y_dim, z_dim;
            if (Nx != 1)
                x_dim = output.addDim("x", Nx);
            if (Ny != 1)
                y_dim = output.addDim("y", Ny);
            if (Nz != 1)
                z_dim = output.addDim("z", Nz);
            NcDim t_dim = output.addDim("time", 1);

            // Define the Cartesian coordinate variables
            NcVar x_grid, y_grid, z_grid;
            if (Nx != 1)
                x_grid = output.addVar("x", ncDouble, x_dim);
            if (Ny != 1)
                y_grid = output.addVar("y", ncDouble, y_dim);
            if (Nz != 1)
                z_grid = output.addVar("z", ncDouble, z_dim);
            NcVar t_grid = output.addVar("time", ncDouble, t_dim);

            // Define units attributes for cartesian coordinate vars. This attaches a
            // text attribute to each of the coordinate variables, containing
            // the units
            if (Nx != 1)
                x_grid.putAtt("units", "meters");
            if (Ny != 1)
                y_grid.putAtt("units", "meters");
            if (Nz != 1)
                z_grid.putAtt("units", "meters");

            // Define the curvilinear coordinate variable
            NcVar zc_grid;
            if (mapped_grid)
            {
                vector<NcDim> dim_vector;
                if (!vm.count("keep-orient"))
                {
                    if (Ny > 1)
                    {
                        dim_vector.push_back(z_dim);
                        dim_vector.push_back(y_dim);
                        dim_vector.push_back(x_dim);
                    } else {
                        dim_vector.push_back(z_dim);
                        dim_vector.push_back(x_dim);
                    }
                }
                else
                {
                    if (Ny > 1)
                    {
                        dim_vector.push_back(x_dim);
                        dim_vector.push_back(z_dim);
                        dim_vector.push_back(y_dim);
                    } else {
                        dim_vector.push_back(x_dim);
                        dim_vector.push_back(z_dim);
                    }
                }
                zc_grid = output.addVar("zc", ncDouble, dim_vector);
                zc_grid.putAtt("units", "meters");
                zc_grid.putAtt("long_name", "vertical coordinate");
            }

            // Find the time
            double time = plot_interval * lexical_cast<double>(itr->first);
            if (time < 3600)
                t_grid.putAtt("units", "seconds");
            else if (time < 86400)
            {
                time /= 3600;     // 60*60
                t_grid.putAtt("units", "hours");
            }
            else
            {
                time /= 86400;    // 60*60*24
                t_grid.putAtt("units", "days");
            }
            t_grid.putVar(&time);

            // Create and save the rectilinear grids
            // x and y are the same in both curvilinear and rectilinear
            if (Nx != 1)
            {
                double *dim = new double[Nx];
                for (size_t i=0; i < Nx; ++i)
                {
                    if ("NO_SLIP" == type_x)
                        dim[i] = min_x + Lx * 0.5 * (1 - cos(M_PI * i / (Nx - 1)));
                    else
                        dim[i] = min_x + Lx * (i + 0.5) / Nx;
                }
                x_grid.putVar(dim);
                delete dim;
            }

            if (Ny != 1)
            {
                double* dim = new double[Ny];
                for (size_t i=0; i < Ny; ++i)
                {
                    if ("NO_SLIP" == type_y)
                        dim[i] = min_y + Ly * 0.5 * (1 - cos(M_PI * i / (Ny - 1)));
                    else
                        dim[i] = min_y + Ly * (i + 0.5) / Ny;
                }
                y_grid.putVar(dim);
                delete dim;
            }

            // Rectilinear vertical grid (always write)
            if (Nz != 1)
            {
                double* dim = new double[Nz];
                for (size_t i=0; i < Nz; ++i)
                {
                    if ("NO_SLIP" == type_z)
                        dim[i] = min_z + Lz * 0.5 * (1 - cos(M_PI * i / (Nz - 1)));
                    else
                        dim[i] = min_z + Lz * (i + 0.5) / Nz;
                }
                z_grid.putVar(dim);
                delete dim;
            }

            // curvilinear vertical grid
            if (mapped_grid)
            {
                ostringstream file;
                file << "zgrid";
                output.putAtt("description", desc.str());
                if (!vm.count("silent"))
                    cout << "\t- "
                         << setw(10)
                         << left
                         << file.str()
                         << "... "
                         << std::flush;

                double* data_ptr = NULL;
                mapped_file mfr, mfw;
                mapped_file_params params;
                if (vm.count("use-mmap-file"))
                {
                    params.path = file.str();
                    params.flags = mapped_file::readonly;
                    mfr.open(params);
                    data_ptr = (double*)mfr.const_data();
                }
                else
                {
                    std::fstream is(file.str().c_str()
                    ,   std::ios::in
                    |   std::ios::binary);
                    data_ptr = new double[Nx * Ny * Nz];
                    is.read((char*)data_ptr, Nx * Ny * Nz * sizeof(double));
                    is.close();
                }

                if (!vm.count("keep-orient"))
                {
                    double* swapped = NULL;
                    if (vm.count("use-mmap-file"))
                    {
                        params.new_file_size = Nx * Ny * Nz * sizeof(double);
                        params.path = file.str() + ".tmp";
                        params.flags = mapped_file::readwrite;
                        mfw.open(params);
                        swapped = (double*)mfw.const_data();
                    }
                    else
                        swapped = new double[Nx * Ny * Nz];

                    if (vm.count("use-mmap-file"))
                    {
                        if (vm.count("silent"))
                        {
                            for (size_t n=0; n < Nx * Ny * Nz; ++n)
                                swapped[n]
                            =   data_ptr[((n%Nx)*Nz*Ny)
                            +   (n/(Nx*Ny)*Ny)
                            +   ((n/Nx)%Ny)];
                        }
                        else
                        {
                            boost::progress_display show_progress(Nx * Ny * Nz);
                            for (size_t n=0; n < Nx * Ny * Nz; ++n, ++show_progress)
                                swapped[n]
                            =   data_ptr[((n%Nx)*Nz*Ny)
                            +   (n/(Nx*Ny)*Ny)
                            +   ((n/Nx)%Ny)];
                        }
                        data_ptr = swapped;
                        mfr.close();
                    }
                    else
                    {
                        #pragma omp parallel for
                        for (size_t n=0; n < Nx * Ny * Nz; ++n)
                            swapped[n]
                        =   data_ptr[((n%Nx)*Nz*Ny)
                        +   (n/(Nx*Ny)*Ny)
                        +   ((n/Nx)%Ny)];
                        delete data_ptr;
                        data_ptr = swapped;
                        if (vm.count("keep-temporary-files"))
                        {
                            string tmp_file = file.str() + ".tmp";
                            std::fstream os(tmp_file.c_str()
                            ,   std::ios::out
                            |   std::ios::binary);
                            os.write((char*)data_ptr, Nx * Ny * Nz * sizeof(double));
                            os.close();
                        }
                    }
                }
                zc_grid.putVar(data_ptr);

                if (!vm.count("silent"))
                    cout << "\b\b\b\bdone"
                         << endl;
            }

            vector<string> files = itr->second;
            for (size_t i=0; i < files.size(); ++i)
            {
                // skipping input fields that are not requested
                if (vm.count("input-fields")
                &&  find(input_fields.begin()
                ,   input_fields.end()
                ,   files[i])
                ==  input_fields.end())
                    continue;

                ostringstream file;
                file << files[i] << '.' << itr->first;
                if (!vm.count("silent"))
                    cout << "\t- " 
                         << setw(10)
                         << left
                         << file.str()
                         << "... "
                         << std::flush;

                double* data_ptr = NULL;
                mapped_file mfr, mfw;
                mapped_file_params params;
                if (vm.count("use-mmap-file"))
                {
                    params.path = file.str();
                    //params.length = 512; // default: complete file
                    params.flags = mapped_file::readonly;
                    mfr.open(params);
                    data_ptr = (double*)mfr.const_data();
                }
                else
                {
                    std::fstream is(file.str().c_str()
                    ,   std::ios::in
                    |   std::ios::binary);
                    data_ptr = new double[Nx * Ny * Nz];
                    is.read((char*)data_ptr, Nx * Ny * Nz * sizeof(double));
                    is.close();
                }

                if (!vm.count("keep-orient"))
                {
                    double* swapped = NULL;
                    if (vm.count("use-mmap-file"))
                    {
                        params.new_file_size = Nx * Ny * Nz * sizeof(double);
                        params.path = file.str() + ".tmp";
                        params.flags = mapped_file::readwrite;
                        mfw.open(params);
                        swapped = (double*)mfw.const_data();
                    }
                    else
                        swapped = new double[Nx * Ny * Nz];

                    if (vm.count("use-mmap-file"))
                    {
                        if (vm.count("silent"))
                        {
                            for (size_t n=0; n < Nx * Ny * Nz; ++n)
                                swapped[n]
                            =   data_ptr[((n%Nx)*Nz*Ny)
                            +   (n/(Nx*Ny)*Ny)
                            +   ((n/Nx)%Ny)];
                        }
                        else
                        {
                            boost::progress_display show_progress(Nx * Ny * Nz);
                            for (size_t n=0; n < Nx * Ny * Nz; ++n, ++show_progress)
                                swapped[n]
                            =   data_ptr[((n%Nx)*Nz*Ny)
                            +   (n/(Nx*Ny)*Ny)
                            +   ((n/Nx)%Ny)];
                        }
                        data_ptr = swapped;
                        mfr.close();
                    }
                    else
                    {
                        #pragma omp parallel for
                        for (size_t n=0; n < Nx * Ny * Nz; ++n)
                            swapped[n]
                        =   data_ptr[((n%Nx)*Nz*Ny)
                        +   (n/(Nx*Ny)*Ny)
                        +   ((n/Nx)%Ny)];
                        delete data_ptr;
                        data_ptr = swapped;
                        if (vm.count("keep-temporary-files"))
                        {
                            string tmp_file = file.str() + ".tmp";
                            std::fstream os(tmp_file.c_str()
                            ,   std::ios::out
                            |   std::ios::binary);
                            os.write((char*)data_ptr, Nx * Ny * Nz * sizeof(double));
                            os.close();
                        }
                    }
                }

                // Define the netCDF variables for the x, y, z data
                vector<NcDim> dim_vector;
                dim_vector.push_back(t_dim);
                if (1 == Nx)
                {
                    if (!vm.count("keep-orient"))
                    {
                       dim_vector.push_back(z_dim);
                       dim_vector.push_back(y_dim);
                    }
                    else
                    {
                       dim_vector.push_back(y_dim);
                       dim_vector.push_back(z_dim);
                    }
                }
                else if (1 == Ny)
                {
                    if (!vm.count("keep-orient"))
                    {
                       dim_vector.push_back(z_dim);
                       dim_vector.push_back(x_dim);
                    }
                    else
                    {
                       dim_vector.push_back(x_dim);
                       dim_vector.push_back(z_dim);
                    }
                }
                else if (1 == Nz)
                {
                    if (!vm.count("keep-orient"))
                    {
                       dim_vector.push_back(y_dim);
                       dim_vector.push_back(x_dim);
                    }
                    else
                    {
                       dim_vector.push_back(x_dim);
                       dim_vector.push_back(y_dim);
                    }
                }
                else
                {
                    if (!vm.count("keep-orient"))
                    {
                       dim_vector.push_back(z_dim);
                       dim_vector.push_back(y_dim);
                       dim_vector.push_back(x_dim);
                    }
                    else
                    {
                       dim_vector.push_back(x_dim);
                       dim_vector.push_back(z_dim);
                       dim_vector.push_back(y_dim);
                    }
                }
                NcVar data = output.addVar(files[i], ncDouble, dim_vector);

                // determining unit
                string unit;
                if (files[i] == "u"
                ||  files[i] == "v"
                ||  files[i] == "w")
                    unit = "m/s";
                else if (files[i] == "b"
                ||  files[i] == "q")
                    unit = "1/s";
                else if (files[i] == "rho")
                    unit = "kg/L";
                else if (files[i] == "epv")
                    unit = "1/s^3";
                else if (files[i] == "psi")
                    unit = "m^2/s";
                else
                    unit = "arb.";
                data.putAtt("units", unit);

                // Write the coordinate variable data to the file
                data.putVar(data_ptr);

                mfw.close();
                mfr.close();
                if (!vm.count("use-mmap-file"))
                    delete data_ptr;
                if (!vm.count("keep-orient")
                &&   vm.count("use-mmap-file")
                &&  !vm.count("keep-temporary-files"))
                    boost::filesystem::remove(file.str() + ".tmp");

                if (!vm.count("silent"))
                    cout << "\b\b\b\bdone"
                            << endl;
            }
        }
        if (!vm.count("silent"))
            cout << "Finished!"
                 << endl
                 << endl;
    }
    catch (invalid_command_line_syntax& e)
    {
        UNREFERENCED_PARAMETER(e);
        cout << visible << endl;
        cout << "Invalid command line syntax!" << endl;
    }
    catch (unknown_option& e)
    {
        UNREFERENCED_PARAMETER(e);
        cout << visible << endl;
        cout << "Unrecognized option!" << endl;
    }
    catch (too_many_positional_options_error& e)
    {
        UNREFERENCED_PARAMETER(e);
        cout << visible << endl;
        cout << "More than one input file must be separated by comma!" << endl;
    }
    catch (invalid_option_value& e)
    {
        UNREFERENCED_PARAMETER(e);
        cout << visible << endl;
        cout << "Invalid option value!" << endl;
    }
    catch (invalid_argument& e)
    {
        UNREFERENCED_PARAMETER(e);
        cout << visible << endl;
        cout << "Wrong argument!" << endl;
    }
    catch (const bad_lexical_cast& e)
    {
        cerr << endl << "Error converting time index: " << e.what() << endl;
    }
    catch(NcException& e)
    {
        cerr << endl << "Error writing NetCDF file: " << e.what() << endl;
    }
    catch(bad_alloc& ba)
    {
        UNREFERENCED_PARAMETER(ba);
        cerr << endl
                << "Error: Cannot allocate memory: "
                << "using memory mapped file (-m command line option) may solve the problem"
                << endl;
    }
    catch (exception& e)
    {
        cerr << endl << "Error: " << e.what() << endl;
    }
}
