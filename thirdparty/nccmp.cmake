set(NCCMP_PREFIX nccmp)
set(NCCMP_URL http://prdownloads.sourceforge.net/nccmp/nccmp-1.8.2.1.tar.gz)
set(NCCMP_URL_MD5 a657f60e01e3bbcbc92d8414f079889f)

if (MSVC)
    set(NCCMP_MAKE msbuild)
else()
    set(NCCMP_MAKE make)
endif()

set(NCPU 2)

ExternalProject_Add(${NCCMP_PREFIX}
    PREFIX ${NCCMP_PREFIX}
    URL ${NCCMP_URL}
    URL_MD5 ${NCCMP_URL_MD5}
    CONFIGURE_COMMAND "./configure"
    BUILD_COMMAND ${NCCMP_MAKE} -j${NCPU} NCCMP_build_prefix=${NCCMP_PREFIX}
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND ""
    LOG_DOWNLOAD 1
    LOG_BUILD 1
    STEP_TARGETS ${NCCMP_PREFIX}_info
)

ExternalProject_Get_Property(${NCCMP_PREFIX} SOURCE_DIR)

option(INSTALL_NCCMP "Install nccmp binary ?" ON)
if (INSTALL_NCCMP)
    install(PROGRAMS ${SOURCE_DIR}/src/${NCCMP_PREFIX} DESTINATION bin
                COMPONENT ${NCCMP_PREFIX})
endif()
