# spins-tools
A set of tools for converting SPINS' (https://wiki.math.uwaterloo.ca/fluidswiki/index.php?title=SPINS_User_Guide) output format to self-describing data formats (e.g. NetCDF and HDF5).

This is part of the Dedicated Programming #563 (DP_563).

Check INSTALL.txt for a full description of how to install spins-tools.
