with import <nixpkgs> { };

stdenv.mkDerivation rec {
  version = "2.1.2.0";
  name = "spins-tools-${version}";

  src = fetchurl {
    url = "https://git.sharcnet.ca/DP/spins-tools/repository/archive.tar.gz?ref=${version}";
    sha256 = "0a0h1whl8cv36mna7xf18a6maygxb4iaaz9x4d0x8qkflmfanwa7";
  };

  cmakeFlags = [
    "-DUSE_NCCMP=0"
    "-DNETCDF_INCLUDE_DIR=${netcdf}/include"
    "-DNETCDF_LIBRARY=${netcdf}/lib/libnetcdf.so"
    "-DNETCDF_CXX_INCLUDE_DIR=${netcdfcxx4}/include"
    "-DNETCDF_CXX_LIBRARY=${netcdfcxx4}/lib/libnetcdf_c++4.so"
  ];

  buildInputs = [
    cmake
    boost
    perl
    netcdf
    netcdfcxx4
  ];

  meta = with stdenv.lib; {
    homepage = https://git.sharcnet.ca/DP/spins-tools;
    description = "A set of tools for converting SPINS' output format to self-describing data formats (e.g. NetCDF)";
    platforms = platforms.all;
    license = licenses.gpl3;
  };
}
